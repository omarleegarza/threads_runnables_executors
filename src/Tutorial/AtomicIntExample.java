package Tutorial;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;


public class AtomicIntExample {
    AtomicInteger count = new AtomicInteger();

    void increment() {
        count.incrementAndGet();
    }

    public static void main(String[] args) throws InterruptedException {
        AtomicIntExample ai = new AtomicIntExample();
        ExecutorService myService3 = Executors.newFixedThreadPool(10);
        IntStream.range(0, 10).forEach(i -> myService3.submit(() -> {
            ai.increment();
            System.out.println(Thread.currentThread().getName() + ai.count.get() );
        }));


        myService3.shutdown();
        myService3.awaitTermination(1, TimeUnit.MINUTES);
        System.out.println("final counter = " + ai.count.get());

    }
}
