package Wk8;

import java.util.Random;

public class RunnableExample implements Runnable {
     String name;
     String book;
     int pages;
     int readTime;
     int rand;



    public RunnableExample(String name, String book, int pages, int readTime) {
        this.name = name;
        this.book = book;
        this.pages = pages;
        this.readTime = readTime;

        Random random = new Random();
        this.rand = random.nextInt(1000);

    }

    public void run() {

        System.out.println("\n Name of Person:"+ name +" Name of Book:"+ book + " pages: "
                + pages + " Average read time in hours:" + readTime + "\n");
        for (int i = 1; i<rand; i++ ){
            if (i % readTime == 0 ){
                System.out.print(name + " is reading ,");
                try {
                    Thread.sleep(readTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.print("\n"+name + " is done reading \n");
    }
}