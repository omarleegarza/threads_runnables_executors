package Wk8;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Executor {

    public static void main(String[] args) {

        ExecutorService myService1 = Executors.newFixedThreadPool(5);

        RunnableExample re1 = new RunnableExample("Omar", "Lucky Man", 600, 14);
        RunnableExample re2 = new RunnableExample("Lee", "Happy Fool", 550, 12);
        RunnableExample re3 = new RunnableExample("Garza", "Red Man", 400, 9);
        RunnableExample re4 = new RunnableExample("Julio", "Blue Bird", 300, 7);

        myService1.execute(re1);
        myService1.execute(re2);
        myService1.execute(re3);
        myService1.execute(re4);

        myService1.shutdown();


    }
}
